FROM openjdk:8-jdk-alpine
ADD /build/libs/lazy-load-0.0.1-SNAPSHOT.jar lazy-load-backend.jar
EXPOSE 9090
ENTRYPOINT [ "java", "-jar", "/lazy-load-backend.jar" ]
