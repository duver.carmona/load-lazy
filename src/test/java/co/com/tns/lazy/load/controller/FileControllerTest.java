package co.com.tns.lazy.load.controller;

import co.com.tns.lazy.load.exception.BusinessException;
import co.com.tns.lazy.load.service.FileService;
import co.com.tns.lazy.load.util.FileUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileControllerTest {

    @InjectMocks
    FileController fileController;

    @Mock
    FileService fileService;

    @Test
    public void shouldUploadAnswerOfTheService() throws BusinessException, IOException {
        String name = "file.txt";
        String originalFileName = "file.txt";
        String contentType = "text/plain";
        byte[] content = null;
        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);
        when(fileService.maximizeElementsByDay(Mockito.any())).thenReturn("Case #1: 1");
        fileController.upload(result);
        Mockito.verify(fileService).maximizeElementsByDay(Mockito.any());
    }
}
