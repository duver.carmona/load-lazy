package co.com.tns.lazy.load.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LazyLoadTest {
	@InjectMocks
	public LazyLoad lazyLoad;

	@Mock
	public Trip trip;

	@Test
	public void shouldMustSeparateAList() {
		List<Integer> fileInTypeList = Arrays.asList(4, 4, 30, 29, 12, 1, 2, 3, 4, 3, 9, 10, 11, 5, 45, 46, 47, 48, 49);
		List<Integer> tripsByDayExpected = Arrays.asList(30, 29, 12, 1);
		List<Integer> tripsByDayResult = lazyLoad.separateList(fileInTypeList);
		assertEquals(tripsByDayExpected, tripsByDayResult);
	}

	@Test
	public void shouldReturnStringMaximizeByDay() {
		List<Integer> fileInTypeList = Arrays.asList(5, 4, 30, 30, 1, 1, 3, 20, 20, 20, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 6, 9, 19, 29, 39, 49, 59, 10, 32, 56, 76, 8, 44, 60, 47, 85, 71, 91);
		when(trip.getNumberOfTrips(Mockito.anyList())).thenReturn(5);
		String maximizeElementsByDayResult = lazyLoad.maximizeElementsByDay(fileInTypeList);
		String maximizeElementsByDayExpected = "Case #1: 5\n" + "Case #2: 5\n" + "Case #3: 5\n" + "Case #4: 5\n" + "Case #5: 5\n";
		assertEquals(maximizeElementsByDayExpected, maximizeElementsByDayResult);
	}

}
