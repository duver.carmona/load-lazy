package co.com.tns.lazy.load.util;

public class Constants {

    public static final String SYMBOL_DOT = ".";
	public static final String VALID_EXTENSION_FILE = ".TXT";

    private Constants() {
		super();
	}

	public static final String FILE_ERROR = "Error en el archivo.";
	public static final String FILE_ERROR_NULL = "Error, el archivo es nulo";
	public static final String FILE_ERROR_EMPTY = "Error, el archivo esta vacio";
	public static final String FILE_ERROR_LECTURE = "Error en la lectura del archivo.";
	public static final String FILE_ERROR_NO_NUMBER = "Error en la lectura del archivo, hay datos no numéricos.";
	public static final String FILE_ERROR_TYPE_NOT_TXT = "Error en el tipo de archivo";
	public static final String BASE_PATH = "lazyLoad";
	public static final String CASES_NUMBER = "Case #";
	public static final String SYMBOL_COLON = ":";
	public static final String SPACE = " ";
	public static final String LINE_BREAK = "\n";
	public static final String ERROR_CREATING_FILE = "Hubo un error con";
}
